﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MySql.Data.EntityFramework;

using Microsoft.AspNet.Identity.EntityFramework;

using Datmon_online2.Models.Initializer;

namespace Datmon_online2.Models.Contexts
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class ProductContext : DbContext
    {
        public ProductContext() : base("DaCap")
        {
            Database.SetInitializer(new ProductDatabaseInitializer());
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CartItem> ShoppingCartItems { get; set; }
    }
}