﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Datmon_online2.Models
{
    public class User 
    {
        [Key,Display(AutoGenerateField = true,Name = "ID")]
        [ScaffoldColumn(false)]
        public int UserID { get; set; }

        [Required, StringLength(40), Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required, StringLength(20), Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required, StringLength(50), Display(Name = "First Name")]
        public string Email { get; set; }
    }
}