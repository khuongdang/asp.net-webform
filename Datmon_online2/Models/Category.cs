﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Datmon_online2.Models
{
    public class Category
    {
        [Key, Display(Name = "ID")]
        [ScaffoldColumn(false)]
        public int CategoryID { get; set; }

        [Required, StringLength(100), Display(Name = "Name")]
        public string CategoryName { get; set; }

        [Display(Name = "Catagory Description")]
        public string Description { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public bool Deleted { get; set; } = false;

        public Category Copy( Category category )
        {
            if (category.CategoryName != null)
                CategoryName = category.CategoryName;

            if (category.Description != null)
                Description = category.Description;

            if (category.Products != null)
                Products = category.Products;

            return this;
        }
    }
}