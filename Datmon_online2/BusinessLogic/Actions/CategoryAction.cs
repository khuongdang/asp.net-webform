﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Datmon_online2.Models;
using Datmon_online2.Models.Contexts;

namespace Datmon_online2.BusinessLogic.Actions
{
    public class CategoryAction : IDisposable
    {
        private ProductContext m_db = new ProductContext();

        public Category AddCategory(Category category)
        {
            Category c = new Category();
            c.Copy(category);
            m_db.Categories.Add(c);
            m_db.SaveChanges();
            return c;
        }

        public Category EditCategory(int id, Category category)
        {
            Category c = GetCategory(id);
            c.Copy(category);
            m_db.SaveChanges();
            return c;
        }

        public Category GetCategory(int id, bool get_Deleted = false)
        {
            Category result = (from c in m_db.Categories
                              where c.CategoryID == id
                              && c.Deleted == (get_Deleted ? c.Deleted : false)
                              select c).FirstOrDefault();
            return result;
        }

        public Category[] GetCategories(bool get_Deleted = false)
        {
            return (from c in m_db.Categories
                    where c.Deleted == (get_Deleted ? c.Deleted : false)
                    select c).ToArray();
        }

        public void DeleteCategory(int id)
        {
            Category c = GetCategory(id);
            c.Deleted = true;

            ProductAction pa = new ProductAction();

            var ps = pa.GetProducts(id);

            foreach( var p in ps)
            {
                pa.DeleteProduct(p.ProductID);
            }
            
            m_db.SaveChanges();
            return;
        }

        public void Dispose()
        {
            if (m_db != null)
            {
                m_db.Dispose();
                m_db = null;
            }
        }
    }
}