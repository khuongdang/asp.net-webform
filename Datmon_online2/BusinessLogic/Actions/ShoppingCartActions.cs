﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

using Datmon_online2.Models;
using Datmon_online2.Models.Contexts;

namespace Datmon_online2.BusinessLogic.Actions
{
    public class ShoppingCartActions : IDisposable
    {
        public string m_ShoppingCartId { get; set; }

        private ProductContext m_db = new ProductContext();

        public const string m_CartSessionKey = "CartId";

        public void AddToCart(int id, int amount = 1)
        {
            m_ShoppingCartId = GetCartId();

            var cartItem = m_db.ShoppingCartItems.SingleOrDefault(
                c => c.CartId == m_ShoppingCartId
                && c.ProductId == id);

            if(cartItem == null)
            {
                cartItem = new CartItem
                {
                    ItemId = Guid.NewGuid().ToString(),
                    ProductId = id,
                    CartId = m_ShoppingCartId,
                    Product = m_db.Products.SingleOrDefault(
                        p => p.ProductID == id),
                    Quantity = amount,
                    DateCreated = DateTime.Now
                };

                m_db.ShoppingCartItems.Add(cartItem);
            }
            else
            {
                cartItem.Quantity+=amount;
            }

            m_db.SaveChanges();

        }

        public void DeleteFromCart(int id, int amount = 1)
        {
            m_ShoppingCartId = GetCartId();

            var cartItem = m_db.ShoppingCartItems.SingleOrDefault(
               c => c.CartId == m_ShoppingCartId
               && c.ProductId == id);

            if (cartItem == null)
            {
                return;
            }
            else
            {
                if (cartItem.Quantity <= amount)
                    m_db.ShoppingCartItems.Remove(cartItem);
                else
                    cartItem.Quantity -= amount;
            }

            m_db.SaveChanges();
        }

        public void Dispose()
        {
            if (m_db != null)
            {
                m_db.Dispose();
                m_db = null;
            }
        }

        public string GetCartId()
        {
            if ( Membership.GetUser() != null)
            {
                return Membership.GetUser().ToString();
            }
            else
            if (HttpContext.Current.Session[m_CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
                {
                    HttpContext.Current.Session[m_CartSessionKey] = HttpContext.Current.User.Identity.Name;
                }
                else
                {
                    // Generate a new random GUID using System.Guid class.     
                    Guid tempCartId = Guid.NewGuid();
                    HttpContext.Current.Session[m_CartSessionKey] = tempCartId.ToString();
                }
            }

            return HttpContext.Current.Session[m_CartSessionKey].ToString();
        }

        public int GetNumberOfCartItems()
        {
            m_ShoppingCartId = GetCartId();

            var quantity = from item in m_db.ShoppingCartItems
                           where item.CartId == m_ShoppingCartId
                           && item.Product.Deleted == false
                           select item.Quantity;
            if (quantity.Count() > 0)
                return quantity.Sum();
            return 0;
        }

        public List<CartItem> GetCartItems()
        {
            m_ShoppingCartId = GetCartId();



            return m_db.ShoppingCartItems.Where(
                c => c.CartId == m_ShoppingCartId
                && c.Product.Deleted == false
                ).ToList();
        }
    }
}