﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

using Datmon_online2.Models;
using Datmon_online2.Models.Contexts;

namespace Datmon_online2.BusinessLogic.Actions
{
    public class ProductAction : IDisposable
    {
        private ProductContext m_db = new ProductContext();

        private string[] get_CurrentUserRoles()
        {
            string[] empty = { };
            var currentUser = Membership.GetUser();
            if (currentUser == null)
                return empty;
            return Roles.GetRolesForUser(currentUser.UserName);
        }

        public Product AddProduct(Product product)
        {
            if (!get_CurrentUserRoles().Contains("Admin"))
                return null;

            Product p = new Product();
            p.Copy(product);
            if (m_db.Products.Add(p) != null)
                m_db.SaveChanges();
            return p;
        }

        public Product EditProduct(int id, Product product)
        {
            if (!get_CurrentUserRoles().Contains("Admin"))
                return null;

            Product pro = GetProduct(id);
            if (pro !=null)
            {
                pro.Copy(product);
                m_db.SaveChanges();
            }
            return pro;
        }

        public Product GetProduct( int id , bool get_Deleted = false)
        {
            Product result = (from p in m_db.Products
                          where p.ProductID == id
                          && p.Deleted == (!get_Deleted ? false : p.Deleted)
                          select p).FirstOrDefault();
            return result;
        }

        public Product[] GetProducts(int Category = -1, bool get_Deleted = false)
        {
            var Products = (from p in m_db.Products
                            where p.CategoryID == (Category == -1 ? p.CategoryID : Category)
                            && p.Deleted == (!get_Deleted ? false : p.Deleted )
                            select p);

            return Products.ToArray();
        }

        public void DeleteProduct(int id)
        {
            if (!get_CurrentUserRoles().Contains("Admin"))
                return;

            Product pro = GetProduct(id);
            if (pro != null)
            {
                pro.Deleted = true;
                m_db.SaveChanges();
                return;
            }
        }

        public void Dispose()
        {
            if (m_db != null)
            {
                m_db.Dispose();
                m_db = null;
            }
        }
    }
}