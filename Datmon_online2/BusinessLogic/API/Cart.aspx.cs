﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Web.Services;

namespace Datmon_online2.BusinessLogic
{
    public partial class Cart : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Contents/Login");
        }

        [WebMethod]
        public static int AddItem(String ProductID, String Amount = "1")
        {
            string rawId = ProductID;
            int productId;
            int amount;
            if (!String.IsNullOrEmpty(rawId) && int.TryParse(rawId, out productId) && int.TryParse(Amount, out amount))
            {
                using (Actions.ShoppingCartActions usersShoppingCart = new Actions.ShoppingCartActions())
                {
                    usersShoppingCart.AddToCart(Convert.ToInt16(rawId), Convert.ToInt16(amount));
                    return usersShoppingCart.GetNumberOfCartItems();
                }
            }
            else
            {              
                return 0;
            }            
        }

        [WebMethod]
        public static int DeleteItem(String ProductID, String Amount = "1")
        {
            string rawId = ProductID;
            int productId;
            int amount;
            if (!String.IsNullOrEmpty(rawId) && int.TryParse(rawId, out productId) && int.TryParse(Amount, out amount))
            {
                using (Actions.ShoppingCartActions usersShoppingCart = new Actions.ShoppingCartActions())
                {
                    usersShoppingCart.DeleteFromCart(Convert.ToInt16(rawId), Convert.ToInt16(amount));
                    return usersShoppingCart.GetNumberOfCartItems();
                }
            }
            else
            {
                return 0;
            }
        }
    }
}