﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CardGroup.ascx.cs" Inherits="Datmon_online2.Controls.CardGroup" %>

<style>
    .category-cards {
        margin:15px;
        min-width:0px;
    }
</style>

<div class="category-cards container-fluid" style="overflow-x:auto; overflow-y:hidden;">
    <h2> <%= m_CategoryName %>
<% if (Roles.IsUserInRole("Admin")) { %>
        <button class="btn btn-primary" onclick="return editCategory(<%: m_CategoryID %>,'<%: m_CategoryName %>');"> Edit</button>
        <asp:Button CssClass="btn btn-danger" ID="bt_Delete" runat="server" Text="Delete" OnClientClick="return confirm('Are your sure ?');" OnClick="bt_Delete_Click" />
<% } %>
    </h2>
    <div class="d-flex flex-row flex-nowrap">
            <asp:PlaceHolder ID="CardsHolder" runat="server"> 
    
            </asp:PlaceHolder>
    </div>
</div>