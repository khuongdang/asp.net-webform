﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Datmon_online2.BusinessLogic.Actions;

namespace Datmon_online2.Controls
{
    public partial class CardGroup : System.Web.UI.UserControl
    {
        List<Card> m_Cardlist = new List<Card>(0);
        public String m_CategoryName;
        public String m_CategoryID;
        int m_NumberOfProducts = 10;

        public void set_Category(String Category, int NumberOfProduct = 10)
        /// Number of product should be shown
        /// 0 to show all
        {
            ProductAction pa = new ProductAction();
            CategoryAction ca = new CategoryAction();

            m_NumberOfProducts = NumberOfProduct;
            m_CategoryID = Category;
            var DBcontext = new Datmon_online2.Models.Contexts.ProductContext();

            int CategoryID = int.Parse(m_CategoryID);
            m_CategoryName = ca.GetCategory(CategoryID).CategoryName;

            
            var Cards = pa.GetProducts( CategoryID );

            if (Cards.Count() > m_NumberOfProducts)
                Cards = Cards.Take(10).ToArray();

            foreach (var card in Cards)
            {
                Card _card = (Card)LoadControl("~/Controls/Card.ascx");
                _card.Set_Product(card);

                m_Cardlist.Add(_card);

                CardsHolder.Controls.Add(_card);
            }
        }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void bt_Delete_Click(object sender, EventArgs e)
        {
            CategoryAction ca = new CategoryAction();
            ca.DeleteCategory(int.Parse(m_CategoryID));
            Response.Redirect(Request.RawUrl);
        }
    }
}