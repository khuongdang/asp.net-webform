﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="Datmon_online2.Controls.Header" %>

<link href="../Css/Header.css" rel="stylesheet" type="text/css" />

<header id="header"  runat="server">
    <nav class="navbar navbar-dark navbar-expand-lg">
         <a class="navbar-brand" href="/">ASP</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdown" aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navbarDropdown" class="collapse navbar-collapse">
            <ul class=" nav navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <% if (Membership.GetUser() != null) { %>              
                 <li class="nav-item"> 
                     <a class="nav-link" href="#" > <%= Membership.GetUser().UserName %> </a>
                 </li>
                <% } %>
                <% if ( Roles.IsUserInRole("Admin") == true) { %>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                          Add
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" onclick="return checkCategoryExist();" href="../Contents/ProductDetail.aspx?productID=0">Add Product</a>
                        <a class="dropdown-item" onclick="addCategory();" >Add Category</a>
                      </div>
                    </li>
                <% } %>
                <li class="nav-item">
                    <asp:UpdatePanel ID ="UP_NumberOfCartItems" runat="server">
                        <ContentTemplate>
                           <a class="button nav-link" style="white-space:nowrap" href="../Contents/ShoppingCart.aspx" > 
                                <b> <%= m_NumberOfCartItems.ToString() %> </b> in Cart                                  
                            </a>
                        </ContentTemplate> 
                     </asp:UpdatePanel>                       
                </li>
                <li class="nav-item">
                    <asp:LoginStatus CssClass="nav-link" ID="LoginStatus1" runat="server" />
                </li>            
            </ul>
        </div>
    </nav> 
</header>

<!-- Modal -->
<% if ( Roles.IsUserInRole("Admin") )
    {  %>
    <div id="AddCategoryModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">       
            <h3 id="Modal_title" class="modal-title">Add Category</h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <asp:TextBox CssClass="form-control" ID="txb_CategoryName" runat="server"></asp:TextBox>
            <asp:HiddenField ID="CategoryID" runat="server" Value="0" />
            <asp:RequiredFieldValidator CssClass="text-danger" runat="server" ControlToValidate="txb_CategoryName" ValidationGroup="AddCategory" Text="This field can not be empty"></asp:RequiredFieldValidator>
          </div>
          <div class="modal-footer">
            <asp:Button CssClass="btn btn-success" ID="bt_AddCategory" ValidationGroup="AddCategory" runat="server" Text="Add" OnClick="bt_AddCategory_Click" />
            <button class="btn btn-dark" data-dismiss="modal">Cancle</button>
          </div>
        </div>
      </div>
    </div>
    
    <script>
        function checkCategoryExist() {
            if (<%= m_number_categories.ToString() %> <= 0)
            {
                alert("You need to add Category!");
                return false;
            }                
            return true;
        }

        function editCategory(id, name) {
            var CategoryID_ID = '#<%: CategoryID.ClientID %>';
            var CategoryName_ID = '#<%: txb_CategoryName.ClientID %>';
            var bt_AddCategory_ID = '#<%: bt_AddCategory.ClientID %>';

            $(CategoryID_ID).attr("value", id);
            $(bt_AddCategory_ID).attr("value", "Edit");
            $(CategoryName_ID).attr("value", name);
            $('#Modal_title').text("Edit Category");       
            $("#AddCategoryModal").modal();
            return false;
        }

        function addCategory() {
            var CategoryID_ID = '#<%: CategoryID.ClientID %>';
            var CategoryName_ID = '#<%: txb_CategoryName.ClientID %>';
            var bt_AddCategory_ID = '#<%: bt_AddCategory.ClientID %>';

            $(CategoryID_ID).attr("value", "0");
            $(bt_AddCategory_ID).attr("value", "Add");
            $(CategoryName_ID).attr("value", "");
            $('#Modal_title').text("Add Category");   
            $("#AddCategoryModal").modal();
            return false;
        }
    </script>

<% } %>

<script type ="text/javascript">
    var UpdatePanel = '<%= UP_NumberOfCartItems.UniqueID %>';

    function UpdateCartHeader()
    {      
        if (UpdatePanel != null)
        {
            __doPostBack(UpdatePanel, '');
        }
    }

    function AddToCardAjax(productid,amount) {
        $.ajax({
            type: 'POST',
            url: '/BusinessLogic/API/Cart.aspx/AddItem',
            data: JSON.stringify({
                ProductID: productid,
                Amount: amount,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            complete: function (data) {
                UpdateCartHeader();
            },
        })
        return false;
    }
</script>

