﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Card.ascx.cs" Inherits="Datmon_online2.Controls.Card" %>

<style>
    .card {
        margin: 15px;
        height: 350px;
        min-width: 240px;
        max-width:240px;
    }
</style>
<div class="card">
  <img id="Image" style="height: 150px;" class="card-img-top" src="/Resources/Images/Logos/404.jpg" alt="Not Found" runat="server" />
  <div class="card-body">
    <a href="../Contents/ProductDetail.aspx?productID=<%=m_product.ProductID%>" > <h5  id="Title" class="card-title" runat="server"> <%= m_product.ProductName %> </h5> </a>
	<div class="font-weight-bold"><%: "$" + m_product.UnitPrice.ToString() %></div>
    <p id="Descriptions" class="card-text" runat="server"> <%= m_product.Description != null ? (m_product.Description.Length>m_Description_lengh ? m_product.Description.Substring(0,m_Description_lengh)+"..." : m_product.Description ):"" %> </p>   
  </div>
  <div class="card-footer">     
      <asp:Button CssClass="btn btn-success" ID="AddToCartButton" runat="server" Text="Add to Cart" AutoPostBack="false"></asp:Button>
  </div>
</div>