﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;

using System.Threading.Tasks;

using Datmon_online2.Models;

namespace Datmon_online2.Controls
{
    public partial class Card : System.Web.UI.UserControl
    {
        public Product m_product = null;
        public int m_Description_lengh = 50;

        public Card() { }
        public Card(Product product)
        {
            m_product = product;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (m_product != null)
            {
                if (System.IO.File.Exists(Server.MapPath("~/Resources/Images/Products/" + m_product.ImagePath)))
                {
                    Image.Src = "/Resources/Images/Products/" + m_product.ImagePath;
                }
                Image.Alt = m_product.Description;

                AddToCartButton.OnClientClick = "AddToCardAjax(" + m_product.ProductID + ",1); return false;";
            }           
        }

        public void Set_Product(Product product)
        {
            m_product = product;
        }       
    }
}