﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Datmon_online2.BusinessLogic.Actions;
using Datmon_online2.Models;

namespace Datmon_online2.Controls
{
    public partial class Header : System.Web.UI.UserControl
    {
        public int m_number_categories { get
            {
                CategoryAction ca = new CategoryAction();
                return ca.GetCategories().Count();
            }
            set { } }
        public int m_NumberOfCartItems
        {
            get
            {
                using (ShoppingCartActions action = new ShoppingCartActions())
                {
                    return action.GetNumberOfCartItems();
                }
            }
            set { }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void bt_AddCategory_Click(object sender, EventArgs e)
        {
            CategoryAction ca = new CategoryAction();
            Category c = new Category();
            c.CategoryName = txb_CategoryName.Text;
            if (CategoryID.Value == "0")
            {
                ca.AddCategory(c);
            }
            else
            {
                ca.EditCategory(int.Parse(CategoryID.Value), c);
            }          
            Response.Redirect(Request.RawUrl);
        }
    }
}