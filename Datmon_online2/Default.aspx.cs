﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Datmon_online2.BusinessLogic.Actions;

using Datmon_online2.Controls;

namespace Datmon_online2.MasterPages
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            CategoryAction ca = new CategoryAction();
            var _Categories = ca.GetCategories();

            foreach (var _Category in _Categories)
            {     
                CardGroup card = (CardGroup)LoadControl("~/Controls/CardGroup.ascx");
                card.set_Category(_Category.CategoryID.ToString() );
                Categories.Controls.Add(card);
            }
        }
    }
}