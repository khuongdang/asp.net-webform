﻿using System;
using System.Linq;
using Datmon_online2.Models;
using System.Web.ModelBinding;
using System.Web.UI.WebControls;
using System.Web.Security;

using Datmon_online2.BusinessLogic.Actions;

namespace Datmon_online2.MasterPages
{
    public partial class ProductDetail : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            CategoryAction ca = new CategoryAction();
            if (ca.GetCategories().Count() <= 0)
                Response.Redirect("../");
        }

        protected void Ddl_CategoriesLoad(object sender, EventArgs e)
        {
            if (productDetail.CurrentMode == FormViewMode.Edit)
            {
                DropDownList Ddl_Categories = (DropDownList)productDetail.FindControl("Ddl_Categories");
                CategoryAction ca = new CategoryAction();
                Ddl_Categories.DataSource = ca.GetCategories();
                Ddl_Categories.DataTextField = "CategoryName";
                Ddl_Categories.DataValueField = "CategoryID";


                if (productDetail.DataItem != null)
                {
                    int? id = (productDetail.DataItem as Product).CategoryID;
                    if (id != null)
                        Ddl_Categories.SelectedValue = id.ToString();
                }
            }
        }

        public Category[] GetCategories(object sender, EventArgs e)
        {
            CategoryAction ca = new CategoryAction();
            return ca.GetCategories();
        }

        public Product GetProduct([QueryString("productID")] int? productId)
        {
            ProductAction pa = new ProductAction();
            Product query;
            if (productId.HasValue && productId > 0)
            {
                query = pa.GetProduct((int)productId);
            }
            else
            {
                if (User.IsInRole("Admin"))
                {
                    query = new Product();
                    query.ImagePath = "404.jpg";
                    productDetail.ChangeMode(FormViewMode.Edit);
                }
                else
                {
                    query = null;
                }
            }
            return query;
        }

        public void ExitButton_Clicked(object sender, EventArgs e)
        {
            productDetail.ChangeMode(System.Web.UI.WebControls.FormViewMode.ReadOnly);
        }

        public void SaveButton_Clicked(object sender, EventArgs e)
        {
            productDetail.UpdateItem(true);
            productDetail.ChangeMode(System.Web.UI.WebControls.FormViewMode.ReadOnly);
        }

        public void EditButton_Clicked(object sender, EventArgs e)
        {
            productDetail.ChangeMode(System.Web.UI.WebControls.FormViewMode.Edit);           
        }

        public void DeleteButton_Clicked(object sender, EventArgs e)
        {
            productDetail.DataBind();
            ProductAction pa = new ProductAction();
            int productID = (productDetail.DataItem as Product).ProductID;
            pa.DeleteProduct(productID);
            Response.Redirect("../");
        }

        public void UpdateProduct(Product product)
        {
            ProductAction pa = new ProductAction();
            int id = product.ProductID;

            Product Query = pa.GetProduct(id);
            FileUpload filecontrol = (FileUpload)productDetail.FindControl("Upload_image");
            DropDownList Ddl_Categories = (DropDownList)productDetail.FindControl("Ddl_Categories");
            if (filecontrol.HasFile)
            {
                string result = ValidateFile();
                if (result != "$INVALID")
                {
                    string filePath = Ultility.AppPath + Ultility.ImagePath;
                    //Todo: add product id
                    string filename = result;
                    filecontrol.SaveAs(filePath + filename);
                    product.ImagePath = filename;
                }
            }

            product.CategoryID = Int32.Parse(Ddl_Categories.SelectedValue);

            if (Query != null)
            {               
                pa.EditProduct(Query.ProductID, product);
            }
            else
            {
                if (User.IsInRole("Admin"))
                {                 
                    pa.AddProduct(product);
                }          
                else
                    return;
            }
            return;
        }

        private string ValidateFile()
        {

            DateTime time = DateTime.Now;
            return time.ToBinary().ToString()+".jpg";
        }

        protected void Ddl_Categories_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}