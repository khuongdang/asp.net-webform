﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" 
    CodeBehind="Login.aspx.cs" Inherits="Datmon_online2.MasterPages.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        height: 33px;
    }
   .tableLogin{
        position: fixed;
        left: 50%;
        top: 50%;
        border-radius:25px;
        padding:20px;
        transform: translate(-50%,-50%);
        background-color: rgb(220, 220, 250);
     }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Login CssClass="" ID="Login1" runat="server" CreateUserText="Sign in" CreateUserUrl="~/Contents/Registration.aspx" MembershipProvider="MySQLMembershipProvider" RenderOuterTable="False">
    <LayoutTemplate>
        <div class="tableLogin">
            <table class="form-group" cellpadding="1" cellspacing="0" style="border-collapse:collapse;" class="tableLogin">
            <tr>
                <td>
                    <table cellpadding="0">
                        <div class="text-center font-weight-bold" style="font-size:30px; margin:10px; margin-bottom:30px;"> Login </div>

                        <tr class="input-group">
                            <td  align="right">
                                <asp:Label CssClass="input-group-text text-dark" style="width:100px" ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox CssClass="form-control" ID="UserName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="text-danger" ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="input-group">
                            <td align="right">
                                <asp:Label CssClass="input-group-text text-dark" style="width:100px" ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="form-control" ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="text-danger" ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" style="color:Red;">
                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2" class="auto-style1">
                                <asp:Button CssClass="btn btn-danger" ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="Login1" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:HyperLink ID="CreateUserLink" runat="server" NavigateUrl="~/Contents/Registration.aspx">Sign in</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </div>       
    </LayoutTemplate>
</asp:Login>
</asp:Content>
