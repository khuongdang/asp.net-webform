﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="Datmon_online2.MasterPages.ProductDetail" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin:20px">
        <asp:FormView ID="productDetail" runat="server" ItemType="Datmon_online2.Models.Product" SelectMethod="GetProduct" RenderOuterTable="false"
        DefaultMode="ReadOnly" EmptyDataText="Such Empty."
        DataKeyNames="ProductID"
        UpdateMethod="UpdateProduct">
        <ItemTemplate>
            <div>
                <h1><%#:Item.ProductName %></h1>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <img src="/Resources/Images/Products/<%#: Item.ImagePath != "" ? Item.ImagePath : "404.jpg" %>" style="border: solid; min-height: 300px; height:300px; width:auto; min-width: 450px" alt="<%#:Item.ProductName %>" />
                    </td>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top; text-align: left;">
                        <b>Description:</b><br />
                        <%#:Item.Description %>
                        <br />
                        <span><b>Price:</b>&nbsp;<%#: String.Format("{0:c}", Item.UnitPrice) %></span><br /><span><b>Product Number:</b>&nbsp;<%#:Item.ProductID %></span><br /><b>Category:</b> <%#:Item.Category.CategoryName %>
                        <br />                     
                        <br />
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Add to card:</span>
                             </div>
                            <input class="form-control" id="ip_amount" type="number" value="1" min="1" />
                            &nbsp;
                            <button class="btn btn-success" id="Bt_Add" runat="server" onclick='<%#: "addtocart(" + Item.ProductID + "); return false;" %>'>Add</button>
                            <% if (Roles.IsUserInRole("Admin")) { %>
                            <asp:Button CssClass="btn btn-primary" ID="bt_Edit" Text="Edit" OnClick="EditButton_Clicked" runat="server" />
                             <% } %>
                        </div>
                    </td>
                </tr>
            </table>
            <script>
                function addtocart(id) {
                    console.log(id);
                    var amount = $("#ip_amount").val();
                    AddToCardAjax(id, amount);
                    return false;
                }
            </script>
        </ItemTemplate>
        <EditItemTemplate>
            <table>
                <tr>
                    <td>
                        <img id="img_Item" src="/Resources/Images/Products/<%#: Item.ImagePath  %>"  style="border: solid; min-height: 300px; height:300px; width:auto; min-width: 450px" alt="<%#:Item.ProductName %>" />
                    </td>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top; text-align: left;">
                        <b>Product Name:</b>                       
                        <nobr />
                        <div class="input-group">
                            <asp:TextBox CssClass="form-control" ID="Txt_ProductName" runat="server" Text='<%# Bind("ProductName") %>' />
                            <div class="input-group-append">
                                <asp:RequiredFieldValidator CssClass="input-group-text" ID="RequiredFieldValidator1" runat="server" ControlToValidate="Txt_ProductName" ValidationGroup="Edit_Item" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <br />
                        <b>Description:</b><br />
                        <div class="input-group">
                            <asp:TextBox CssClass="form-control" ID="Txt_Description" TextMode="MultiLine" runat="server" Columns="50" Text='<%# Bind("Description") %>' />
                            <div class="input-group-append">
                                 <asp:RequiredFieldValidator CssClass="input-group-text" ID="RequiredFieldValidator2" runat="server" ControlToValidate="Txt_ProductName" ValidationGroup="Edit_Item" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <br />
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Price</div>
                            </div>
                            <asp:TextBox CssClass="form-control" ID="Txt_UnitPrice" runat="server" TextMode="Number" value="0" min="0" step="0.01" ValidateRequestMode="Disabled" Text='<%# Bind("UnitPrice") %>' />
                             <div class="input-group-append">
                                <div class="input-group-text">$</div>
                            </div>
                        </div>
                            <br />
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Category</div>
                            </div>
                            <asp:DropDownList CssClass="form-control" ID="Ddl_Categories" OnLoad="Ddl_CategoriesLoad" runat="server" ></asp:DropDownList>                  
                        </div>
                        <br />
                        <span><b>Product Number:</b>&nbsp;  <%#:Item.ProductID %></span>
                        <br />
                        <b>Upload Image:</b><asp:FileUpload CssClass="btn" ID="Upload_image" runat="server" />
                        <br /><br />
                        <asp:Button CssClass="btn btn-success" ID="bt_Save" Text="Save" OnClick="SaveButton_Clicked" runat="server" ValidationGroup="Edit_Item" />
                        <nobr />               
                        <asp:Button CssClass="btn btn-danger" Visible=<%# (Item.ProductID != 0) %> ID="bt_Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this item?');" OnClick="DeleteButton_Clicked" runat="server" />
                        <nobr /> 
                        <asp:Button CssClass="btn btn-dark" ID="bt_Cancle" Text="Cancle" OnClick="ExitButton_Clicked" runat="server" />
                    </td>
                </tr>
            </table>
            <script>
                var UploadID = '#<%= productDetail.FindControl("Upload_image").ClientID %>';
                var DDLID = '#<%= productDetail.FindControl("Ddl_Categories").ClientID %>';

                $(UploadID).change(function () {
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("#img_Item").attr("src", e.target.result);
                        }
                    }

                    reader.readAsDataURL(this.files[0]);
                });
            </script>
        </EditItemTemplate>
    </asp:FormView>
    </div>
</asp:Content>
