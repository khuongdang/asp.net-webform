﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Datmon_online2.BusinessLogic.Actions;
using Datmon_online2.Models;

namespace Datmon_online2.MasterPages
{
    public partial class ShoppingCart : System.Web.UI.Page
    {
        public double m_total_carts = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public List<CartItem> GetShoppingCartItems()
        {
            ShoppingCartActions actions = new ShoppingCartActions();
            List<CartItem> carts = actions.GetCartItems();
            foreach ( CartItem cart in carts )
            {
                m_total_carts += (cart.Product.UnitPrice != null ? (double)cart.Product.UnitPrice : 0 ) * cart.Quantity;
            }
            return actions.GetCartItems();
        }  

        public void OnBtRemove_Click(object sender, EventArgs e)
        {
            ShoppingCartActions actions = new ShoppingCartActions();
            actions.DeleteFromCart(Convert.ToInt32( Modal_id.Value ),Convert.ToInt32(Modal_amount.Value));
            CartList.DataBind();
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            ShoppingCartActions actions = new ShoppingCartActions();
            List<CartItem> carts = actions.GetCartItems();
            foreach (CartItem cart in carts)
            {
                actions.DeleteFromCart(cart.ProductId, cart.Quantity);
            }
            CartList.DataBind();
        }
    }
}