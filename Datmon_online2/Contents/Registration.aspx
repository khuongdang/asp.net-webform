﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Datmon_online2.MasterPages.Registration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .tableLogin{
                position: fixed;
                left: 50%;
                top: 50%;
                border-radius:25px;
                padding:20px;
                transform: translate(-50%,-50%);
                background-color: rgb(220, 220, 250);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tableLogin">
         <asp:CreateUserWizard  ID="CreateUserWizard1" runat="server" MembershipProvider="MySQLMembershipProvider" CreateUserButtonStyle-CssClass="btn btn-success" CompleteSuccessTextStyle-CssClass="text-success" LabelStyle-Width="150px" LabelStyle-CssClass="input-group-prepend text-dark" LabelStyle-VerticalAlign="Middle" TextBoxStyle-CssClass="form-control" ContinueDestinationPageUrl="~/Default.aspx" ForeColor="Red">
<CompleteSuccessTextStyle CssClass="text-success"></CompleteSuccessTextStyle>

<CreateUserButtonStyle CssClass="btn btn-success"></CreateUserButtonStyle>

<LabelStyle VerticalAlign="Middle" CssClass="input-group-prepend text-dark" Width="150px"></LabelStyle>

<TextBoxStyle CssClass="form-control"></TextBoxStyle>
             <TitleTextStyle Font-Bold="True" Font-Size="Large" ForeColor="Black" />
            <WizardSteps>
                <asp:CreateUserWizardStep runat="server" />
                <asp:CompleteWizardStep runat="server" />
            </WizardSteps>
        </asp:CreateUserWizard>
    </div>
</asp:Content>
