﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="Datmon_online2.MasterPages.ShoppingCart" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="ShoppingCartTitle" runat="server" class="ContentHead">
        <h1>Shopping Cart</h1>
    </div>
    <asp:UpdatePanel ID="Up_CartTable" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="CartList" runat="server" AutoGenerateColumns="False" ShowFooter="True" GridLines="Vertical" CellPadding="4"
                ItemType="Datmon_online2.Models.CartItem" SelectMethod="GetShoppingCartItems"
                CssClass="table table-striped table-bordered">
                <Columns>
                    <asp:BoundField DataField="ProductID" HeaderText="ID" SortExpression="ProductID" />
                    <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" Text="<%#Item.Product.ProductName %>" NavigateUrl='<%# "../Contents/ProductDetail.aspx?productID=" + Item.Product.ProductID.ToString() %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Product.UnitPrice" HeaderText="Price (each)" DataFormatString="{0:c}" />
                    <asp:BoundField HeaderText="Quantity" runat="server" DataField="Quantity"></asp:BoundField>
                    <asp:TemplateField HeaderText="Item Total">
                        <ItemTemplate>
                            <%#: String.Format("{0:c}", ((Convert.ToDouble(Item.Quantity)) *  Convert.ToDouble(Item.Product.UnitPrice)))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remove Item">
                        <ItemTemplate>
                            <button
                                id="<%# Item.ProductId %>"
                                class="btn btn-primary" type="button"
                                onclick="toggle_modal(this.id)"
                                data-name="<%# Item.Product.ProductName %>" data-amount="<%# Item.Quantity %>">
                                Remove</button>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div>
                <p></p>
                <strong>
                    <asp:Label ID="LabelTotalText" runat="server" Text="Order Total: "></asp:Label><%:"$"+ m_total_carts.ToString() %>        
                </strong>
                <% if (m_total_carts - 0.0f > 0.001)
                    { %>
                <asp:Button CssClass="btn btn-success" Style="float: right; padding-right: 20px" Text="Orders" runat="server" OnClick="Unnamed1_Click" />
                <% } %>
            </div>
            <br />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Bt_Modal_Remove" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>


    <!-- Modal -->
    <div class="modal fade" id="Remove_modal" tabindex="-1" role="dialog" aria-labelledby="Remove_modal_Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="Remove_modal_Label">Remove From Cart</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input id="Modal_id" type="number" readonly runat="server" />
                    <input id="Modal_name" type="text" readonly runat="server" />
                    <input id="Modal_amount" type="number" min="1" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <asp:Button ID="Bt_Modal_Remove" CssClass="btn btn-primary" OnClick="OnBtRemove_Click" Text="Remove" UseSubmitBehavior="false" data-dismiss="modal" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <script>
        function toggle_modal(id) {
            var button = $("#" + id);
            var name = button.data('name');
            var amount = button.data('amount');
            var modal = $('#Remove_modal');

            var modal_id = '#<%= Modal_id.ClientID %>';
            var modal_name = '#<%= Modal_name.ClientID %>';
            var modal_amount = '#<%= Modal_amount.ClientID %>';

            modal.find('.modal-title').text(' ' + amount);
            modal.find(modal_amount).attr("max", amount);
            modal.find(modal_amount).val(amount);
            modal.find(modal_id).val(id);
            modal.find(modal_name).val(name);

            modal.modal();
            return false;
        }

        $('#Remove_modal').on('show.bs.modal', function (event) {


        })
    </script>
</asp:Content>
